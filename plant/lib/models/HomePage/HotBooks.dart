class HotBook {
  final String title;
  final String imageURL;
  final String price;
  final List<HotBooksContent> contents;
  final String description;
  final String subtitle;

  HotBook({
    required this.title,
    required this.imageURL,
    required this.price,
    required this.contents,
    required this.description,
    required this.subtitle,
  });
}

class HotBooksContent {
  final String title;
  final List<String> subtitle;

  HotBooksContent({
    required this.title,
    required this.subtitle,
  });
}
