class GetStartedModel{
  final String imageURL;
  final String title;

  GetStartedModel({required this.imageURL, required this.title});
}