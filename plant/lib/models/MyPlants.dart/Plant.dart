class Plant {
  final String name;
  final String imagePath;
  final DateTime date;
  final String caption;
  final String lighting;
  final String watering;

  Plant({
    required this.caption,
    required this.lighting,
    required this.watering,
    required this.name,
    required this.date,
    required this.imagePath,
  });
}
