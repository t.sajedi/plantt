import 'package:carousel_slider/carousel_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:plant/models/HomePage/HotBooks.dart';

class BooksCtrl extends GetxController {
  RxInt bookIndex = 0.obs;
  Rx<PageController> pagesController = PageController(
    initialPage: 0,
  ).obs;
  Rx<CarouselController> booksController = CarouselController().obs;

  void changePage(int index){
    Future.delayed(Duration(milliseconds: 100)).then((value) {
      booksController.value.jumpToPage(index);
    });
  }
  //  this list for hot books part
  List hotBooks = [
    HotBook(
      title: "گل هفته گل هفته",
      imageURL:
          "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/gettyimages-743744991-612x612-1610127940.jpg",
      subtitle: "انحصاری",
      description:
          "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است",
      contents: [
        HotBooksContent(
          title: "مختصری از تاریخچه گیاهان آپارتمانی",
          subtitle: [],
        ),
        HotBooksContent(
          title: "نحوه انتخاب گیاه آپارتمانی مناسب",
          subtitle: [],
        ),
        HotBooksContent(
          title: "گالری",
          subtitle: [
            "گیاهان آب گرم",
            "سلطان قلب ها",
            "پای فیل",
            "سرخس دلتا مو",
            "گیاه برگ چروک",
          ],
        ),
      ],
      price: "0",
    ),
    HotBook(
      title:
          " راهنمای کامل باغبانی راهنمای کامل باغبانی راهنمای کامل باغبانی راهنمای کامل باغبانی",
      imageURL:
          "https://images.unsplash.com/photo-1559563362-c667ba5f5480?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxjb2xsZWN0aW9uLXBhZ2V8MXw4MjE3MzMyfHxlbnwwfHx8fA%3D%3D&w=1000&q=80",
      subtitle: "",
      description:
          "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است",
      contents: [
        HotBooksContent(
          title: "هفته 1 : موز",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 2 : خیار",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 3 : بادمجان",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 4 : موز",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 5 : خیار",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 6 : بادمجان",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 7 : موز",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 8 : خیار",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 9 : بادمجان",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 10 : موز",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 11 : خیار",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 12 : بادمجان",
          subtitle: [],
        ),
      ],
      price: "0",
    ),
    HotBook(
      title: "گل هفته",
      imageURL:
          "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/gettyimages-743744991-612x612-1610127940.jpg",
      subtitle: "",
      description:
          "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است",
      contents: [
        HotBooksContent(
          title: "مختصری از تاریخچه گیاهان آپارتمانی",
          subtitle: [],
        ),
        HotBooksContent(
          title: "نحوه انتخاب گیاه آپارتمانی مناسب",
          subtitle: [],
        ),
        HotBooksContent(
          title: "گالری",
          subtitle: [
            "گیاهان آب گرم",
            "سلطان قلب ها",
            "پای فیل",
            "سرخس دلتا مو",
            "گیاه برگ چروک",
          ],
        ),
      ],
      price: "0",
    ),
    HotBook(
      title: "راهنمای کامل باغبانی",
      imageURL:
          "https://images.unsplash.com/photo-1559563362-c667ba5f5480?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxjb2xsZWN0aW9uLXBhZ2V8MXw4MjE3MzMyfHxlbnwwfHx8fA%3D%3D&w=1000&q=80",
      subtitle: "",
      description:
          "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است",
      contents: [
        HotBooksContent(
          title: "هفته 1 : موز",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 2 : خیار",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 3 : بادمجان",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 4 : موز",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 5 : خیار",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 6 : بادمجان",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 7 : موز",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 8 : خیار",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 9 : بادمجان",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 10 : موز",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 11 : خیار",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 12 : بادمجان",
          subtitle: [],
        ),
      ],
      price: "0",
    ),
  ].obs;
}
