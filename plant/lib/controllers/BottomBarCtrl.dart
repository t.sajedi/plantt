import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:plant/views/Camera/CameraAlert.dart';
import 'package:plant/views/Camera/CameraPage.dart';
import 'package:plant/views/Diagnose/DiagnosePage.dart';
import 'package:plant/views/Home/HomaPage.dart';
import 'package:plant/views/More/MorePage.dart';
import 'package:plant/views/MyPlants/MyPlantsPage.dart';

class BottomBarCtrl extends GetxController {
  final page = 0.obs;
  List<Widget> pages = [
    HomePage(),
    DiagnosePage(),
    CameraPage(),
    MyPlantsPage(),
    MorePage(),
  ].obs;
  Rx<PageController> pagesController = PageController(
    initialPage: 0,
  ).obs;

  // @override
  // void onInit() {
  //   pagesController.value = PageController(initialPage: 0);
  //   Future.delayed(Duration(milliseconds: 5000)).then((value) {
  //     log(pagesController.value.page.toString());
  //   });
  //   super.onInit();
  // }

  int findPage(Widget page) {
    return pages.indexOf(page);
  }

  changepage(Widget newpage, int index) {
    int pageindex = findPage(newpage);
    if (pageindex >= 0) {
      pagesController.value.jumpToPage(pageindex);
      log(pagesController.value.page.toString() + "   : page");
      log(index.toDouble().toString());
    } else {
      Get.to(() => newpage);
    }
    // pagesController.value
  }

  changePage(BuildContext context, int newpageIndex) {
    page.value = newpageIndex;
    log(page.value.toString());
    if (page.value == 2) {
      showImageAlert(context);
    }
  }
}
