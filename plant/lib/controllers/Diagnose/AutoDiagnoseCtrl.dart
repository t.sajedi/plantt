import 'package:get/get.dart';

class AutoDiagCtrl extends GetxController {
  RxString wholePlant = "".obs;
  RxString sickPart1 = "".obs;
  RxString sickPart2 = "".obs;
  RxInt picsCount = 0.obs;
  RxList pics = [
    {
      "name": "کل گیاه",
      "image": "",
    }.obs,
    {
      "name": "بخش بیمار",
      "image": "",
    }.obs,
    {
      "name": "بخش بیمار",
      "image": "",
    }.obs,
  ].obs;

  updatephotoes(bool isPlus) {
    if (isPlus) {
      picsCount++;
    } else {
      picsCount--;
    }
  }
}
