import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:plant/Them/Thems.dart';
import 'package:plant/controllers/BooksCtrl.dart';
import 'package:plant/controllers/BottomBarCtrl.dart';
import 'package:plant/controllers/Diagnose/AutoDiagnoseCtrl.dart';
import 'package:plant/controllers/Diagnose/DiagnosePageCtrlr.dart';
import 'package:plant/controllers/ExpertPageCtrl.dart';
import 'package:plant/controllers/HomePageCtrl.dart';
import 'package:plant/controllers/PlantsPageCtrl.dart';
import 'package:plant/views/BottomBar/Widgets/Body.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';

class BottombarPage extends StatelessWidget {
  final ColorsCtrl colorCtrl = Get.put(ColorsCtrl());
  final BooksCtrl booksCtrl = Get.put(BooksCtrl());
  final PlantsPageCtrl plantsPageCtrl = Get.put(PlantsPageCtrl());
  final DiagnosePageCtrl diagnosePageCtrl = Get.put(DiagnosePageCtrl());
  final HomePageCtrl homePageCtrl = Get.put(HomePageCtrl());
  final BottomBarCtrl bottomBarCtrl = Get.put(BottomBarCtrl());
  final AutoDiagCtrl autoDiagCtrl = Get.put(AutoDiagCtrl());
  // final ExpertCtrl expertCtrl = Get.put(ExpertCtrl());
  // Get.lazyPut(()=>ExpertCtrl())
  // PlantsPageCtrl plantsPageCtrl = Get.put(PlantsPageCtrl());

  // @override
  // Widget build(BuildContext context) {
  //   return Scaffold(
  //     body: Container(
  //       height: MediaQuery.of(context).size.height,
  //       width: MediaQuery.of(context).size.width,
  //       child: Stack(
  //         children: [
  //           Obx(() {
  //             return PageView(
  //               controller: bottomBarCtrl.pagesController.value,
  //               physics: NeverScrollableScrollPhysics(),
  //               children: bottomBarCtrl.pages,
  //             );
  //           }),
  //           Positioned(
  //             bottom: 0,
  //             child: Container(
  //               height: 100,
  //               width: MediaQuery.of(context).size.width,
  //               // color: Colors.red,
  //               decoration: BoxDecoration(
  //                 // color: Colors.red,
  //                 image: DecorationImage(
  //                   fit: BoxFit.cover,
  //                   image: AssetImage("assets/bottom.png"),
  //                 ),
  //               ),

  //               child: Row(
  //                 mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //                 crossAxisAlignment: CrossAxisAlignment.end,
  //                 children: [
  //                   InkWell(
  //                     onTap: () {
  //                       bottomBarCtrl.changepage(bottomBarCtrl.pages[0], 0);
  //                     },
  //                     child: Obx(() {
  //                       return tabItem(
  //                         index: 0,
  //                         currentPage:
  //                             bottomBarCtrl.pagesController.value.page ?? 0,
  //                         page: bottomBarCtrl.pages[0],
  //                         icon: FontAwesomeIcons.home,
  //                         title: 'خانه',
  //                       );
  //                     }),
  //                   ),
  //                   InkWell(
  //                     onTap: () {
  //                       bottomBarCtrl.changepage(bottomBarCtrl.pages[1], 1);
  //                     },
  //                     child: Obx(() {
  //                       return tabItem(
  //                         index: 1,
  //                         currentPage:
  //                             bottomBarCtrl.pagesController.value.page ?? 0,
  //                         page: bottomBarCtrl.pages[1],
  //                         icon: FontAwesomeIcons.stethoscope,
  //                         title: 'تشخیص',
  //                       );
  //                     }),
  //                   ),
  //                   InkWell(
  //                     onTap: () {
  //                       bottomBarCtrl.changepage(bottomBarCtrl.pages[2], 2);
  //                     },
  //                     child: Obx(() {
  //                       return tabItem(
  //                         index: 2,
  //                         currentPage:
  //                             bottomBarCtrl.pagesController.value.page ?? 0,
  //                         page: bottomBarCtrl.pages[2],
  //                         icon: Icons.add,
  //                         title: 'اضاقه کردن',
  //                       );
  //                     }),
  //                   ),
  //                   InkWell(
  //                     onTap: () {
  //                       bottomBarCtrl.changepage(bottomBarCtrl.pages[3], 3);
  //                     },
  //                     child: Obx(() {
  //                       return tabItem(
  //                         currentPage:
  //                             bottomBarCtrl.pagesController.value.page ?? 0,
  //                         index: 3,
  //                         page: bottomBarCtrl.pages[3],
  //                         icon: FontAwesomeIcons.seedling,
  //                         title: 'گیاه من',
  //                       );
  //                     }),
  //                   ),
  //                   InkWell(
  //                     onTap: () {
  //                       bottomBarCtrl.changepage(bottomBarCtrl.pages[4], 4);
  //                     },
  //                     child: Obx(() {
  //                       log("hi Obx 4");
  //                       return tabItem(
  //                         index: 4,
  //                         currentPage:
  //                             bottomBarCtrl.pagesController.value.page ?? 0,
  //                         page: bottomBarCtrl.pages[4],
  //                         icon: FontAwesomeIcons.stream,
  //                         title: 'بیشتر',
  //                       );
  //                     }),
  //                   ),
  //                 ],
  //               ),
  //             ),
  //           ),
  //         ],
  //       ),
  //     ),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.red,
      body: Body(),
      bottomNavigationBar: Obx(
        () => ConvexAppBar(
          style: TabStyle.fixedCircle,
          height: 60,
          top: -30,
          curveSize: 100,
          color: colorCtrl.bottomBarActiveColor.value.withOpacity(0.5),
          activeColor: colorCtrl.bottomBarActiveColor.value,
          backgroundColor: colorCtrl.bottomBarColor.value,
          onTap: (int i) {
            print('click index=$i');
            bottomBarCtrl.changePage(context, i);
          },
          items: [
            TabItem(
              icon: FontAwesomeIcons.home,
              title: 'خانه',
            ),
            TabItem(
              icon: FontAwesomeIcons.stethoscope,
              title: 'تشخیص',
            ),
            TabItem(
              icon: Icons.add,
              title: 'Add',
            ),
            TabItem(
              icon: FontAwesomeIcons.seedling,
              title: 'گیاه من',
            ),
            TabItem(
              icon: FontAwesomeIcons.stream,
              title: 'بیشتر',
            ),
          ],
          // initialActiveIndex: bottomBarCtrl.page.value,
        ),
      ),
      //  Obx(
      //   () => BottomNavigationBar(
      //     currentIndex: bottomBarCtrl.page.value,
      //     onTap: (index) {
      //       log("$index");
      //       bottomBarCtrl.changePage(index);
      //     },
      //     items: [
      //       BottomNavigationBarItem(
      //         icon: new Icon(Icons.home),
      //         label: "خانه",
      //       ),
      //       // BottomNavigationBarItem(
      //       //   icon: new Icon(Icons.mail),
      //       //   label: 'تشخیص',
      //       // ),
      //       BottomNavigationBarItem(
      //         icon: new Icon(Icons.mail),
      //         label: 'اضافه',
      //       ),
      //       BottomNavigationBarItem(
      //         icon: Icon(Icons.person),
      //         label: 'گیاه من',
      //       ),
      //       BottomNavigationBarItem(
      //         icon: new Icon(Icons.home),
      //         label: "بیشتر",
      //       ),
      //     ],
      //   ),
      // ),
    );
  }
}
