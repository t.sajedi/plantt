import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:plant/controllers/BottomBarCtrl.dart';
import 'package:plant/controllers/Static.dart';

Widget tabItem({
  required Widget page,
  required String title,
  required IconData icon,
  required int index,
  required double currentPage,
}) {
  final BottomBarCtrl bottomBarCtrl = Get.find();
  // return Obx(() {
  // return InkWell(
  //   onTap: () {
  //     // log("hi : " + index.toString());
  //     bottomBarCtrl.changepage(page, index);
  //   },
  //   child:
  // );
  // });
  return Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    mainAxisAlignment:
        index == 2 ? MainAxisAlignment.center : MainAxisAlignment.end,
    children: [
      // GetX<BottomBarCtrl>(
      //   init: BottomBarCtrl(),
      //   builder: (val) => Text(
      //     '$page',
      //   ),
      // ),bottomBarCtrl.pagesController.value.page == index.toDouble()
      Icon(
        icon,
        size: 24,
        color: currentPage == index ? Colors.green : Colors.grey,
      ),
      SizedBox(
        height: verticmargin * 0.5,
      ),
      Text(
        title,
        textAlign: TextAlign.end,
        style: TextStyle(
          color: Colors.black,
        ),
      ),
    ],
  );
}
