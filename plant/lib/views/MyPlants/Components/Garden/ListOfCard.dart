import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
// import 'package:images_picker/images_picker.dart';
import 'package:plant/controllers/PlantsPageCtrl.dart';
import 'package:plant/controllers/Static.dart';
// import 'package:persian_date/persian_date.dart';
import 'package:jalali_calendar/jalali_calendar.dart';

Widget listOfCars(BuildContext context) {
  PlantsPageCtrl plantsPageCtrl = Get.find();
  PersianDate persianDate = PersianDate();
  return Container(
    height: MediaQuery.of(context).size.height,
    width: MediaQuery.of(context).size.width,
    color: Colors.black12,
    child: Obx(() {
      return ListView.builder(
        padding: EdgeInsets.zero,
        itemCount: plantsPageCtrl.plants.length,
        itemBuilder: (context, index) {
          return Container(
            // color: Colors.red,
            height: 200,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(
              right: horizmargin,
              left: horizmargin,
              bottom: verticmargin,
              top: index == 0 ? verticmargin : 0,
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(12)),
            ),
            child: Column(
              children: [
                Container(
                  height: 150,
                  width: MediaQuery.of(context).size.width,
                  // color: Colors.red,
                  child: Row(
                    textDirection: TextDirection.rtl,
                    children: [
                      Container(
                        height: 150,
                        width: 125,
                        margin: EdgeInsets.only(
                            top: verticmargin, right: horizmargin),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              image: FileImage(
                                File(plantsPageCtrl.plants[index].imagePath),
                              ),
                            )),
                      ),
                      SizedBox(width: horizmargin),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          SizedBox(height: verticmargin),
                          Text(
                            plantsPageCtrl.plants[index].name,
                            textAlign: TextAlign.end,
                            style: TextStyle(
                              fontSize: 24.0,
                              color: Colors.green[900],
                            ),
                          ),
                          Text(
                            plantsPageCtrl.plants[index].caption,
                            textAlign: TextAlign.end,
                            style: TextStyle(
                              fontSize: 22.0,
                              color: Colors.green[700],
                            ),
                          ),
                          Row(
                            textDirection: TextDirection.rtl,
                            children: [
                              Icon(
                                Icons.light_mode,
                                color: Colors.yellow[600],
                                size: 28,
                              ),
                              SizedBox(width: horizmargin * 0.5),
                              Text(
                                plantsPageCtrl.plants[index].lighting,
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                  fontSize: 22.0,
                                  color: Colors.green[700],
                                ),
                              )
                            ],
                          ),
                          Row(
                            textDirection: TextDirection.rtl,
                            children: [
                              Icon(
                                Icons.water,
                                color: Colors.blue[600],
                                size: 28,
                              ),
                              SizedBox(width: horizmargin * 0.5),
                              Text(
                                plantsPageCtrl.plants[index].watering,
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                  fontSize: 22.0,
                                  color: Colors.green[700],
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(height: verticmargin * 0.5),
                Container(
                  height: 0.8,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.grey[200],
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: horizmargin),
                    // color: Colors.red,
                    child: Row(
                      textDirection: TextDirection.rtl,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "اضافه شده در " +
                              persianDate.gregorianToJalali(
                                plantsPageCtrl.plants[index].date.year,
                                plantsPageCtrl.plants[index].date.month,
                                plantsPageCtrl.plants[index].date.day,
                                " ",
                              ),
                          textAlign: TextAlign.end,
                          style: TextStyle(
                              color: Colors.green[900], fontSize: 18.0),
                        ),
                        Icon(
                          Icons.add_photo_alternate_outlined,
                          size: 28,
                          color: Colors.green[700],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      );
    }),
  );
}
