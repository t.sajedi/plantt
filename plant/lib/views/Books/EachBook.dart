import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:plant/controllers/BooksCtrl.dart';
import 'package:plant/controllers/Static.dart';
import 'package:plant/views/Books/Components/contents.dart';
import 'package:plant/views/Books/Components/price.dart';

import 'Components/Slider.dart';
import 'Components/Titile.dart';

// ignore: must_be_immutable
class EachBook extends StatelessWidget {
  BooksCtrl booksCtrl = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            // appBar
            Container(
              height: 55,
              width: MediaQuery.of(context).size.width,
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                textDirection: TextDirection.rtl,
                children: [
                  IconButton(
                    icon: Icon(
                      FontAwesomeIcons.arrowRight,
                    ),
                    onPressed: () {
                      Get.back();
                    },
                  ),
                  // SizedBox(
                  //   width: horizmargin,
                  // ),
                  Container(
                    width: MediaQuery.of(context).size.width -
                        horizmargin -
                        horizmargin -
                        30,
                    // color: Colors.blue,
                    // padding: const EdgeInsets.all(4),
                    alignment: Alignment.centerRight,
                    child: Obx(() {
                      return Text(
                        booksCtrl.hotBooks[booksCtrl.bookIndex.value].title,
                        textAlign: TextAlign.end,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: 28,
                        ),
                      );
                    }),
                  ),
                  Spacer()
                ],
              ),
            ),
            Expanded(
              child: Obx(() {
                return ListView(
                  children: [
                    slider(context),
                    booksCtrl.hotBooks[booksCtrl.bookIndex.value].subtitle != ""
                        ? Container(
                            height: 30,
                            width: MediaQuery.of(context).size.width,
                            color: Colors.brown,
                            alignment: Alignment.center,
                            child: Text(
                              booksCtrl
                                  .hotBooks[booksCtrl.bookIndex.value].subtitle,
                              textAlign: TextAlign.end,
                              style: TextStyle(
                                fontSize: 21,
                                color: Colors.white,
                              ),
                            ),
                          )
                        : Container(),
                    title(
                      context,
                      booksCtrl.hotBooks[booksCtrl.bookIndex.value].title,
                    ),
                    price(
                      context,
                    ),
                    // contents(
                    //   context,
                    // ),
                  ],
                );
              }),
            ),
          ],
        ),
      )),
    );
  }
}
