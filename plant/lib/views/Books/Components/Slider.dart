import 'dart:developer';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:plant/controllers/BooksCtrl.dart';
import 'package:plant/controllers/Static.dart';

Widget slider(BuildContext context) {
  BooksCtrl booksCtrl = Get.find();
  return Container(
    // height: 240,
    width: MediaQuery.of(context).size.width,
    color: Colors.grey[200],
    padding: EdgeInsets.only(top: verticmargin),
    child: Column(
      children: [
        Obx(() {
          return CarouselSlider.builder(
            carouselController: booksCtrl.booksController.value,
            options: CarouselOptions(
                // autoPlay: true,
                aspectRatio: 2 / 1,
                // pageSnapping:
                enlargeCenterPage: true,
                enlargeStrategy: CenterPageEnlargeStrategy.scale,
                reverse: true,
                enableInfiniteScroll: false,
                onPageChanged: (index, hi) {
                  log(booksCtrl.bookIndex.value.toString());
                  booksCtrl.bookIndex.value = index;
                  log(booksCtrl.bookIndex.value.toString());
                }),
            itemCount: booksCtrl.hotBooks.length,
            itemBuilder: (
              BuildContext context,
              int itemIndex,
              int pageViewIndex,
            ) {
              return Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(12)),
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    colorFilter: new ColorFilter.mode(
                      Colors.black.withOpacity(0.2),
                      BlendMode.darken,
                    ),
                    image: NetworkImage(
                      booksCtrl.hotBooks[itemIndex].imageURL,
                    ),
                  ),
                ),
                child: Column(
                  mainAxisAlignment: ((itemIndex % 2) == 0)
                      ? MainAxisAlignment.end
                      : MainAxisAlignment.start,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.symmetric(
                        horizontal: horizmargin,
                        vertical: verticmargin,
                      ),
                      child: Text(
                        // homePageCtrl.hotBooks[index].title,
                        booksCtrl.hotBooks[itemIndex].title,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16.0,
                          fontWeight: FontWeight.w600,
                        ),
                        textAlign: TextAlign.right,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                ),
              );
            },
          );
        }),
        SizedBox(height: verticmargin * 0.6),
        Container(
          height: 32,
          width: 120,
          decoration: BoxDecoration(
            color: Colors.blue,
            borderRadius: BorderRadius.all(
              Radius.circular(18),
            ),
          ),
          alignment: Alignment.center,
          child: Text(
            "خواندن نمونه",
            textAlign: TextAlign.end,
            style: TextStyle(
              fontSize: 20,
              color: Colors.white,
            ),
          ),
        ),
        SizedBox(height: verticmargin * 0.6),
      ],
    ),
  );
}
