import 'package:flutter/material.dart';
import 'package:plant/controllers/Static.dart';

Widget title(BuildContext context, String title) {
  return Container(
    height: 70,
    width: MediaQuery.of(context).size.width,
    margin: EdgeInsets.symmetric(horizontal: horizmargin),
    // color: Colors.red,
    child: Text(
      title ,
      textAlign: TextAlign.end,
      maxLines: 2,
      overflow: TextOverflow.ellipsis,
      style: TextStyle(
        fontSize: 24,
        color: Colors.black,
      ),
    ),
  );
}
