import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:plant/controllers/ExpertPageCtrl.dart';
import 'package:plant/controllers/Static.dart';
import 'package:plant/views/Experts/Components/Appbar.dart';
import 'package:plant/views/Experts/Components/btn.dart';

class ExpertsPage extends StatelessWidget {
  final ExpertCtrl expertCtrl = Get.put(ExpertCtrl());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          appBar(context),
          Container(
            height: 400,
            width: MediaQuery.of(context).size.width,
            // color: Colors.red,
            child: Obx(() {
              return CarouselSlider.builder(
                itemCount: expertCtrl.msages.length,
                options: CarouselOptions(
                  autoPlay: true,
                  // enlargeCenterPage: true,
                  disableCenter: true,
                  viewportFraction: 1.0,
              // enlargeCenterPage: false,
                  onPageChanged: (index, reason) {
                    expertCtrl.currentPage.value = index;
                  },
                ),
                itemBuilder: (ontext, index, realIdx) {
                  return Container(
                    width: MediaQuery.of(context).size.width,
                    // color: Colors.blue,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "تصویر را باز کنید",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.w600,
                            color: Colors.black,
                          ),
                        ),
                        Text(
                          "گیاهان زیبا را کشف کنید",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 24,
                            // fontWeight: FontWeight.w600,
                            color: Colors.black,
                          ),
                        ),
                        Container(
                          height: 176,
                          width: 176,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage(expertCtrl.msages[index].img),
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                        Text(
                          expertCtrl.msages[index].txt,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.w600,
                            color: Colors.black,
                          ),
                        ),
                      ],
                    ),
                  );
                },
              );
            }),
          ),
          Obx(() {
            return Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: expertCtrl.msages.asMap().entries.map((entry) {
                return GestureDetector(
                  // onTap: () => _controller.animateToPage(entry.key),
                  child: Container(
                    width: 12.0,
                    height: 12.0,
                    margin:
                        EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color(0xff3ecb62).withOpacity(
                          expertCtrl.currentPage.value == entry.key
                              ? 0.9
                              : 0.4),
                    ),
                  ),
                );
              }).toList(),
            );
          }),
          Spacer(),
          Text(
            "اشتراک سالانه فقط 30 هزار تومان",
            textAlign: TextAlign.right,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              fontSize: 26,
              color: Colors.black,
            ),
          ),
          SizedBox(height: verticmargin * 0.4),
          continueBtn(context),
          SizedBox(height: verticmargin),
        ],
      ),
    );
  }
}
