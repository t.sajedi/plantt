import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:plant/controllers/Static.dart';
import 'package:plant/views/Experts/ExpertsPage.dart';

Widget askExperts(BuildContext context) {
  return Container(
    height: 300,
    width: MediaQuery.of(context).size.width,
    margin: EdgeInsets.symmetric(
      horizontal: horizmargin,
      vertical: verticmargin,
    ),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Text(
          "ارتباط با کارشناسان",
          textAlign: TextAlign.end,
          style: TextStyle(
            fontSize: 24.0,
            color: Colors.green[900],
            fontWeight: FontWeight.w600,
          ),
        ),
        Expanded(
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12.0),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: horizmargin,
                vertical: verticmargin,
              ),
              child: Column(
                children: [
                  Container(
                    height: 150,
                    // color: Colors.red,
                    child: Row(
                      textDirection: TextDirection.rtl,
                      children: [
                        Container(
                          height: 150,
                          width: MediaQuery.of(context).size.width * 0.32,
                          // color: Colors.red,
                          // margin: ,
                          decoration: BoxDecoration(
                            // color: Colors.red,
                            borderRadius: BorderRadius.all(Radius.circular(12)),
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage(
                                "https://media.istockphoto.com/photos/biologist-in-a-forest-alone-picture-id945377578?k=20&m=945377578&s=612x612&w=0&h=zO3gIK54g3K-ALh1lZfGKk3JOgdp3Zg5vx8st9obs_U=",
                              ),
                            ),
                          ),
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width -
                                  MediaQuery.of(context).size.width * 0.32 -
                                  56 -
                                  24,
                              // color: Colors.black,
                              margin: EdgeInsets.symmetric(
                                horizontal: horizmargin,
                                // vertical: verticmargin,
                              ),
                              child: Text(
                                "مشاوره فردی با متخصصان",
                                textAlign: TextAlign.end,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                                style: TextStyle(
                                  color: Colors.green[800],
                                  // fontWeight: FontWeight.w600,
                                  fontSize: 24,
                                ),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width -
                                  MediaQuery.of(context).size.width * 0.32 -
                                  56 -
                                  24,
                              // color: Colors.black,
                              margin: EdgeInsets.symmetric(
                                horizontal: horizmargin,
                                // vertical: verticmargin,
                              ),
                              child: Text(
                                // "مشاوره فردی با متخصصان",
                                "راه حل های دقیق برای مشکلات باغبانی خود دریافت کنید",
                                textAlign: TextAlign.end,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                                style: TextStyle(
                                  color: Colors.green[300],
                                  // fontWeight: FontWeight.w600,
                                  fontSize: 20,
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: verticmargin),
                  Expanded(
                    child: Center(
                      child: InkWell(
                        onTap: () {
                          log("Ask Experts");
                          Get.to(ExpertsPage());
                        },
                        child: Container(
                          height: 50,
                          margin: EdgeInsets.symmetric(
                              horizontal: horizmargin * 2.5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            // color: Colors.green,
                            gradient: LinearGradient(
                              begin: Alignment.centerLeft,
                              end: Alignment.centerRight,
                              colors: [Color(0xff31a689), Color(0xff3ecb62)],
                            ),
                          ),
                          child: Center(
                            child: Text(
                              // "تشخیص خودکار",
                              "ارتباط با کارشناسان",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 24,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    ),
  );
}
