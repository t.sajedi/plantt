import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:plant/controllers/Static.dart';
import 'package:plant/views/Diagnose/Pages/AutoDiagnose/AutoDiagPage.dart';

Widget autoDiagnose(BuildContext context) {
  return Padding(
    padding: EdgeInsets.symmetric(
      horizontal: horizmargin,
    ),
    child: Card(
      elevation: 2.0,
      // color: Colors.transparent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
      ),
      child: Container(
        height: 300,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            SizedBox(height: verticmargin),
            Container(
              height: 100,
              width: MediaQuery.of(context).size.width * 0.6,
              // width: 100,
              // color: Colors.red,
              decoration: BoxDecoration(
                // color: Colors.red,
                image: DecorationImage(
                  image: AssetImage("assets/autdiag.png"),
                  fit: BoxFit.fitHeight,
                ),
              ),
            ),
            SizedBox(height: verticmargin),
            Text(
              "از قسمت های بیمار گیاهان خود از زاویه های مختلف عکس بگیرید",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                color: Colors.green[700],
                fontSize: 24,
              ),
            ),
            SizedBox(height: verticmargin),
            InkWell(
              onTap: () {
                SystemChrome.setSystemUIOverlayStyle(
                  SystemUiOverlayStyle(
                      statusBarColor: Colors.white,
                      statusBarIconBrightness: Brightness.dark),
                );
                Get.to(AutoDiagnosePage())!.then((value) {
                  SystemChrome.setSystemUIOverlayStyle(
                    SystemUiOverlayStyle(
                        statusBarColor: Colors.transparent,
                        statusBarIconBrightness: Brightness.dark),
                  );
                });
              },
              child: Container(
                height: 50,
                margin: EdgeInsets.symmetric(horizontal: horizmargin * 2.5),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                  // color: Colors.green,
                  gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [Color(0xff31a689), Color(0xff3ecb62)],
                  ),
                ),
                child: Center(
                  child: Text(
                    "تشخیص خودکار",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 24,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}
