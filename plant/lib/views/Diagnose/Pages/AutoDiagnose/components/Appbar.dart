import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:plant/controllers/Static.dart';

Widget appBarAutoDiag(BuildContext context) {
  return SafeArea(
    child: Container(
      color: Colors.white,
      height: 65,
      width: MediaQuery.of(context).size.width,
      child: Row(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.end,
      textDirection: TextDirection.rtl,
      children: [
        IconButton(
          icon: Icon(
            FontAwesomeIcons.arrowRight,
          ),
          onPressed: () {
            Get.back();
          },
        ),
        SizedBox(width: horizmargin,),
        Padding(
          padding: const EdgeInsets.all(4),
          child: Text(
            "تشخیص",
            textAlign: TextAlign.end,
            style: TextStyle(
              fontSize: 28,
            ),
          ),
        ),
        Spacer()
      ],
    ),
    ),
  );
}
