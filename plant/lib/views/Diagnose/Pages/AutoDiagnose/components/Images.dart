import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:plant/controllers/Diagnose/AutoDiagnoseCtrl.dart';
import 'package:images_picker/images_picker.dart';
import 'package:plant/controllers/Static.dart';

Widget cardImage(BuildContext context, Map object) {
  AutoDiagCtrl autoDiagCtrl = Get.find();
  return Container(
      margin: EdgeInsets.all(4),
      // color: Colors.red,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8)),
        color: Colors.grey[300],
        image: object["image"] == ""
            ? null
            : DecorationImage(
                fit: BoxFit.cover,
                image: FileImage(
                  File(
                    object["image"].toString(),
                  ),
                ),
              ),
      ),
      child: object["image"] == ""
          ? Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(
                  Icons.add,
                  size: 20,
                ),
                Text(
                  object["name"],
                  textAlign: TextAlign.center,
                ),
              ],
            )
          : Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                InkWell(
                  onTap: (){
                    object["image"] = "";
                    autoDiagCtrl.updatephotoes(false);
                  },
                  child: Container(
                    height: 20,
                    width: 20,
                    margin: EdgeInsets.only(
                      top: verticmargin * 0.4,
                      right: horizmargin,
                    ),
                    decoration: BoxDecoration(
                      // color: Colors.red,
                      shape: BoxShape.circle,
                    ),
                    child: Center(
                      child: Icon(
                        Icons.cancel,
                        color: Colors.red[300],
                      ),
                    ),
                  ),
                )
              ],
            ));
}

showAutoImageAler({required BuildContext context, required int index}) {
  AutoDiagCtrl autoDiagCtrl = Get.find();
  showDialog(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: new Text("لطفا عکس گیاه مورد نظر را انتخاب کنید"),
        // content: new Text("Alert Dialog body"),
        actions: <Widget>[
          OutlinedButton(
            onPressed: () async {
              log("camera");
              List<Media>? res = await ImagesPicker.openCamera(
                pickType: PickType.image,
                // quality: 0.5,
                cropOpt: CropOption(
                  aspectRatio: CropAspectRatio.custom,
                ),
                // maxTime: 60,
              );
              if (res != null) {
                log(res[0].path);
                autoDiagCtrl.pics[index]["image"] = res[0].path;
                autoDiagCtrl.updatephotoes(true);
                // setState(() {
                // path = res[0].thumbPath;
                // });
              }
              Navigator.pop(context);
            },
            child: Text("دوربین"),
          ),
          OutlinedButton(
            onPressed: () async {
              List<Media>? res = await ImagesPicker.pick(
                count: 1,
                pickType: PickType.image,
                language: Language.System,
                // maxSize: 500,
                cropOpt: CropOption(
                  aspectRatio: CropAspectRatio.custom,
                ),
              );
              if (res != null) {
                print(res.map((e) => e.path).toList());
                log(res[0].thumbPath.toString());
                autoDiagCtrl.pics[index]["image"] = res[0].thumbPath;
                autoDiagCtrl.updatephotoes(true);

                // setState(() {
                // path = res[0].thumbPath;
                // });
                // bool status = await ImagesPicker.saveImageToAlbum(File(res[0]?.path));
                // print(status);
              }
              Navigator.pop(context);
            },
            child: Text("گالری"),
          ),
          // Spacer()
        ],
      );
    },
  );
}
