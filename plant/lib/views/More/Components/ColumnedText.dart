import 'package:flutter/material.dart';

Widget columnedText({required String title, required String num}) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Text(
        num,
        style: TextStyle(
          fontSize: 24,
          color: Colors.white,
        ),
      ),
      Text(
        title,
        style: TextStyle(
          fontSize: 20,
          color: Colors.white,
        ),
      ),
    ],
  );
}
