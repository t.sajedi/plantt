import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:plant/controllers/HomePageCtrl.dart';
import 'package:plant/controllers/Static.dart';

Widget popularPlants(BuildContext ctx) {
  HomePageCtrl homePageCtrl = Get.find();
  return Container(
    height: 550,
    width: MediaQuery.of(ctx).size.width,
    margin: EdgeInsets.only(top: verticmargin),
    // color: Colors.red,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        title(),
        SizedBox(
          height: verticmargin * 0.4,
        ),
        Expanded(
          child: Obx(() {
            return GridView.builder(
              itemCount: homePageCtrl.popUlarPlants.length,
              physics: NeverScrollableScrollPhysics(),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: 5/3,
              ),
              itemBuilder: (context, index) {
                return Container(
                  height: 55 - verticmargin,
                  margin: EdgeInsets.only(
                      bottom: verticmargin,
                      right: horizmargin / 2,
                      left: horizmargin / 2),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(12.0)),
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      colorFilter: new ColorFilter.mode(
                          Colors.black.withOpacity(0.3), BlendMode.darken),
                      image: NetworkImage(
                        homePageCtrl.popUlarPlants[index].imageURL,
                      ),
                    ),
                  ),
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                      padding: EdgeInsets.all(horizmargin * 1.2),
                      child: Text(
                        homePageCtrl.popUlarPlants[index].title,
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w400,
                          fontSize: 22,
                        ),
                      ),
                    ),
                  ),
                );
              },
            );
          }),
        ),
      ],
    ),
  );
}

Widget title() {
  return Container(
    margin: EdgeInsets.only(right: horizmargin * 1.8),
    child: Text(
      "گیاهان محبوب",
      style: TextStyle(
        fontSize: 24,
        fontWeight: FontWeight.w700,
        color: Colors.green[800],
      ),
    ),
  );
}
