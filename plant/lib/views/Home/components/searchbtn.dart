import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:plant/controllers/Static.dart';

Widget searchBtn() {
  return Container(
    margin: EdgeInsets.symmetric(horizontal: horizmargin),
    height: 50.0,
    decoration: BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.all(Radius.circular(30)),
    ),
    child: InkWell(
      onTap: () {
        log("hi");
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "جست و جو",
            style: TextStyle(color: Colors.black87, fontSize: 28.0),
          ),
          SizedBox(
            width: 24,
          ),
          Icon(
            FontAwesomeIcons.search,
          ),
        ],
      ),
    ),
  );
}
