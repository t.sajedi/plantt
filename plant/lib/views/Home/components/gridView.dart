import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:plant/controllers/HomePageCtrl.dart';
import 'package:plant/controllers/Static.dart';

Widget centerGridView({required BuildContext ctx}) {
  HomePageCtrl homePageCtrl = Get.find();
  return Container(
    height: 200,
    width: MediaQuery.of(ctx).size.width,
    margin: EdgeInsets.symmetric(horizontal: horizmargin),
    decoration: BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.all(
        Radius.circular(12),
      ),
    ),
    child: Obx(() {
      return GridView.builder(
        itemCount: homePageCtrl.btnsList.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: homePageCtrl.btnsList.length ~/ 2,
        ),
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              log("$index");
              log(homePageCtrl.btnsList[index].pageIndex.toString());
              if (homePageCtrl.btnsList[index].pageIndex is Container) {
                log("yes");
              } else {
                Get.to(homePageCtrl.btnsList[index].pageIndex);
              }
              // bottomBarCtrl.changePage(context,i);
              // Get.to(homePageCtrl.btnsList[index].pageIndex);
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(
                  homePageCtrl.btnsList[index].icon,
                  color: homePageCtrl.btnsList[index].iconColor,
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  homePageCtrl.btnsList[index].title,
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.green[900],
                    fontWeight: FontWeight.w600,
                  ),
                )
              ],
            ),
          );
        },
      );
    }),
  );
}
